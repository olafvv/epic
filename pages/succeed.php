<!DOCTYPE html>
<html>
    <head>
        <link href="../css/bootstrap.min.css" rel="stylesheet" type="text/css">
        <link rel="stylesheet" type="text/css" href="../css/custom.css">
        <link href='https://fonts.googleapis.com/css?family=Ubuntu:400,700italic,500italic,400italic,300italic,700,500,300' rel='stylesheet' type='text/css'>
        <link rel="shortcut icon" type="image/png" href="../img/favicon.png"/>    
        
        <script type="text/javascript" src="../script.js"></script>
        
        <title>Register | Epic Collaboration Zone</title>
    </head>
    <body>   <?php include('html-includes/navigation-nomenu.html'); ?> 

    <div class="container">
        <div class="main">  
          
            
            <div class="col-lg-8 succeed">
                <h2>Registration succesful</h2>
                <p>Thank you for registering! You're one step away to share your Epic ideas. You will receive an email with an activation link to activate your account. Please check your inbox.</p>
                <p>Click the button below to return to the home page.</p>
                <a href="../index.php" class="button-sq-md">Return</a>
            </div>
            
        
        </div>
    
        <?php include('html-includes/footer.html'); ?>
    </div>
    </body>
</hmtl>