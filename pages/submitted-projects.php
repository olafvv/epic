<!DOCTYPE html>
<html>
    <head>
        <title>Register | Epic Collaboration Zone</title>
       
        <link rel="shortcut icon" type="image/png" href="../img/favicon.png"/>    
        
        <!-- user stylesheets -->
        <link href='https://fonts.googleapis.com/css?family=Ubuntu:400,700italic,500italic,400italic,300italic,700,500,300' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" type="text/css" href="../css/custom.css">
        
        <!-- Bower stylesheets -->
        <link href="../bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" type="text/css">

        <!-- Bower scripts -->
        <script src="../bower_components/jquery/dist/jquery.min.js"></script>
        <script src="../bower_components/bootstrap/dist/js/bootstrap.js"></script>

    </head>
<body>       
<?php include('html-includes/navigation-nomenu.html'); ?>
 
    <div class="main">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                </div>
            </div>  
        </div>
    </div>
  <?php include('html-includes/footer.html'); ?>
</body>
</hmtl>