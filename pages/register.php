<!DOCTYPE html>
<html>
    <head>
        <link href="../css/bootstrap.min.css" rel="stylesheet" type="text/css">
        <link rel="stylesheet" type="text/css" href="../css/custom.css">
        <link href='https://fonts.googleapis.com/css?family=Ubuntu:400,700italic,500italic,400italic,300italic,700,500,300' rel='stylesheet' type='text/css'>
        <link rel="shortcut icon" type="image/png" href="../img/favicon.png"/>    
        
        <link href="../bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" type="text/css">
        <script src="../bower_components/jquery/dist/jquery.min.js"></script>
        <script src="../bower_components/bootstrap/dist/js/bootstrap.js"></script>
      
        <script type="text/javascript" src="../js/register.js"></script>
        
        <title>Register | Epic Collaboration Zone</title>
    </head>
<body>
    <?php include('html-includes/navigation-nomenu.html'); ?>
    <div class="main">
        <div class="container">
            <!-- startrow -->
            <div class="row">
               <div class="col-md-12">
                    <div class="col-md-6 col-md-offset-3" id="register-form">
                        <h2>Registration form</h2>
                        <form class="myForm" id="register" action="../action/registration.php" method="post">
                            <input type="text" name="name" placeholder="Name" class="text">
                            <input type="email" name="email" placeholder="Email address" class="text">
                            <input type="password" name="password" placeholder="Password, at least 6 characters" class="text">
                            <input type="password" name="password-confirm" placeholder="Confirm password" class="text">
                            <input type="text" name="role" placeholder="Role" class="text">
                            <input type="checkbox" name="terms">I agree to the terms and conditions<br>
                            <input type="submit" name="submit" class="button-sq-md">
                        </form>
                    </div>
                </div>
            </div>
            <!-- endrow -->
            
        </div>
    </div>
    <?php include('html-includes/footer.html'); ?>
</body>
</hmtl>