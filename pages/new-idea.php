<!DOCTYPE html>
<html>
<head>
<title>New idea | Epic Collaboration Zone</title>

		<link rel="shortcut icon" type="image/png" href="../img/favicon.png"/>    
		
		<!-- user stylesheets -->
		<link href='https://fonts.googleapis.com/css?family=Ubuntu:400,700italic,500italic,400italic,300italic,700,500,300' rel='stylesheet' type='text/css'>
		<link rel="stylesheet" type="text/css" href="../css/custom.css">
		
		<!-- Bower stylesheets -->
		<link href="../bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" type="text/css">

		<!-- Bower scripts -->
		<script src="../bower_components/jquery/dist/jquery.min.js"></script>
		<script src="../bower_components/bootstrap/dist/js/bootstrap.js"></script>
</head>
<body>
	<?php include('html-includes/navigation-nomenu.html'); ?>
	
	<div class="main">
		<div class="container">
			<!-- startrow -->
			<div class="row">
			   <div class="col-md-12">
					<div class="col-md-6 col-md-offset-3" id="new-idea-form">
						<h2>Submit idea</h2>
						<form class="myForm" id="new-idea" action="" method="post">
							
							<label>Idea name</label>
							<input type="text" name="name" placeholder="Idea name" class="text">

							<label> Originator</label>
							<input type="email" name="email" placeholder="Email address" class="text">
							
							<label>Alternative Contact</label>
							<input type="text" name="alt-contact" placeholder="Alternative contact" class="text">

							<label>Mission Statement</label>
							<input type="text" name="mission-statement" placeholder="Mission statement" class="text">
							
							<label>Idea Description</label>
							<textarea class="form-control" rows="3" name="description" placeholder="Description"></textarea>

							<label>Product Owner</label>
							<input type="text" name="product-owner" placeholder="Product owner" class="text">

							<label>Archetypical Client</label>
							<input type="text" name="arch-client" placeholder="Archetypical client" class="text">

							<label>Urgency</label>
							<input type="text" name="urgency" placeholder="Urgency" class="text">

							<label>Tehcnology / Platform</label>
							<input type="text" name="technology" placeholder="Technology / Platform" class="text">

							<label>Progress</label>
							<input type="text" name="progress" placeholder="Progress" class="text">

							<input type="submit" name="submit" class="button-sq-md" value="Submit idea">
						</form>
					</div>
				</div>
			</div>
			<!-- endrow -->
		</div>
	</div>
	 <?php include('html-includes/footer.html'); ?>
	</body>
</hmtl>


 			