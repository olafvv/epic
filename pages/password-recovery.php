<!DOCTYPE html>
<html>
    <head>
        <link href="../css/bootstrap.min.css" rel="stylesheet" type="text/css">
        <link rel="stylesheet" type="text/css" href="../css/custom.css">
        <link href='https://fonts.googleapis.com/css?family=Ubuntu:400,700italic,500italic,400italic,300italic,700,500,300' rel='stylesheet' type='text/css'>
        <link rel="shortcut icon" type="image/png" href="../img/favicon.png"/>    
        
        <script type="text/javascript" src="../script.js"></script>
        
        <title>Register | Epic Collaboration Zone</title>
    </head>
    <body>
    <div class="container">
        <div class="main">
            
            <?php include('html-includes/navigation-nomenu.html'); ?>
            
            <div class="col-lg-5 register-form">
                <h2>Password recovery</h2>
                <p>Please fill in your email address below to receive a link to reset your password.</p>
                <form class="myForm" action="../action/pw-recovery-link.php" method="post">
                    <input type="email" name="email" placeholder="Email address" class="text">
                    <input type="submit" name="submit">
                </form>
            </div>
            
        
        </div>
    
        <?php include('html-includes/footer.html'); ?>
    </div>
    </body>
</hmtl>