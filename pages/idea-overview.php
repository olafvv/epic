<!DOCTYPE html>
<html>
	<head>
		<title>Register | Epic Collaboration Zone</title>
	   
	   	<link rel="shortcut icon" type="image/png" href="../img/favicon.png"/>    
	   	
	   	<!-- user stylesheets -->
		<link href='https://fonts.googleapis.com/css?family=Ubuntu:400,700italic,500italic,400italic,300italic,700,500,300' rel='stylesheet' type='text/css'>
		<link rel="stylesheet" type="text/css" href="../css/custom.css">
		
		<!-- Bower stylesheets -->
		<link href="../bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" type="text/css">

		<!-- Bower scripts -->
		<script src="../bower_components/jquery/dist/jquery.min.js"></script>
		<script src="../bower_components/bootstrap/dist/js/bootstrap.js"></script>

	</head>
<body>
	<div class="main">
		<div class="container">
			<?php include('html-includes/navigation-nomenu.html'); ?>
			
			<!-- Startrow filters -->
			<div class="row">
				<div class="col-md-12" id="overview-filters">

					<!-- Search input field -->
					<div class="col-md-3">
						<div class="input-group">
							<input type="text" class="form-control" placeholder="Search ideas">
							<span class="input-group-btn"> 
								<button class="btn btn-default" type="button">Search</button> 
							</span>
						</div>
					<!-- END SEARCH INPUT FIELD -->
					</div>
				
					<!-- pagination -->
					<div class="col-md-3 col-md-offset-2">
							<ul class="pagination" id="overview-pagination">
								<li class="disabled">
								  <span>
									<span aria-hidden="true">&laquo;</span>
								  </span>
								</li>
								<li class="active">
								  <span>1 <span class="sr-only">(current)</span></span>
								</li>
								<li>
									<span>2</span>
								</li>
								<li>
									<span>3</span>
								</li>
								<li>
									<span>4</span>
								</li>
								<li>
									<a href="#" aria-label="Next">
										<span aria-hidden="true">&raquo;</span>
									</a>
								</li>
							</ul>
					</div><!-- END PAGINATION -->

					<!-- START SORTING -->
					<div class="col-md-4">
						<div class="sorting">
							<div class="btn-group">
								<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
									Sort ideas by <span class="caret"></span>
								</button>

								<ul class="dropdown-menu">
									<li><a href="#">Date ascending</a></li>
									<li><a href="#">Date descending</a></li>
									<li><a href="#">Popularity</a></li>
								</ul>
							</div>		

							<div class="btn-group">
								  <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
									Status<span class="caret"></span>
								  </button>
							</div>

							<ul class="dropdown-menu">
								<li><a href="#">New</a></li>
								<li><a href="#">Under review</a></li>
								<li><a href="#">On hold</a></li>
							</ul>
						</div>	
					<!-- END SORTING -->
					</div>
			<!-- End overview filters -->
			</div>
			<!-- endrow filters -->
			</div>

			<!-- startrow projects -->
			<div class="row">
				<!-- Projects row -->
				<div class="col-md-12 projects-holder">
					 <!-- First card -->        
					<div class="col-md-4 project-column">
						<div class="col-md-12  project-card">
							<a href="idea-detail.php">
								<img src="../img/u22.png" class="idea-img-preview">

								<h3>Lorem ipsum dolor a sit amet, constectetur a dipiscing elit.</h3>
							</a>
						   
						   <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et vi.</p>

						   <span class="glyphicon glyphicon-thumbs-up" aria-hidden="true"></span>
						</div>
					</div>    
					<!-- End of first Card -->

					<!--   Copy pasta --> 
					<div class="col-md-4  project-column">
						<div class="col-md-12  project-card">
							<img src="../img/u22.png" class="idea-img-preview">

							<h3>Lorem ipsum dolor a sit amet, constectetur a dipiscing elit.</h3>
						   
						   <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et vi.</p>
							<span class="glyphicon glyphicon-thumbs-up" aria-hidden="true"></span>
						</div>
					</div>   
		
					<div class="col-md-4 project-column">
						<div class="col-md-12  project-card">
							<a href="idea-detail.php    ">
								<img src="../img/u22.png" class="idea-img-preview">

								<h3>Lorem ipsum dolor a sit amet, constectetur a dipiscing elit.</h3>
							</a>
						   
						   <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et vi.</p>

						   <span class="glyphicon glyphicon-thumbs-up" aria-hidden="true"></span>
						</div>
					</div>    

					<div class="col-md-4 project-column">
							<div class="col-md-12  project-card">
								<a href="idea-detail.php">
									<img src="../img/u22.png" class="idea-img-preview">

									<h3>Lorem ipsum dolor a sit amet, constectetur a dipiscing elit.</h3>
								</a>
							   
							   <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et vi.</p>

							   <span class="glyphicon glyphicon-thumbs-up" aria-hidden="true"></span>
							</div>
					</div>     

					<div class="col-md-4  project-column">
							<div class="col-md-12  project-card">
								<img src="../img/u22.png" class="idea-img-preview">

								<h3>Lorem ipsum dolor a sit amet, constectetur a dipiscing elit.</h3>
							   
							   <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et vi.</p>
								<span class="glyphicon glyphicon-thumbs-up" aria-hidden="true"></span>
							</div>
					</div>   
		
					<div class="col-md-4 project-column">
						<div class="col-md-12  project-card">
							<a href="idea-detail.php    ">
								<img src="../img/u22.png" class="idea-img-preview">

								<h3>Lorem ipsum dolor a sit amet, constectetur a dipiscing elit.</h3>
							</a>
						   
						   <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et vi.</p>

						   <span class="glyphicon glyphicon-thumbs-up" aria-hidden="true"></span>
						</div>
					</div>    

					<div class="col-md-4 project-column">
						<div class="col-md-12  project-card">
							<a href="idea-detail.php">
								<img src="../img/u22.png" class="idea-img-preview">

								<h3>Lorem ipsum dolor a sit amet, constectetur a dipiscing elit.</h3>
							</a>
						   
						   <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et vi.</p>

						   <span class="glyphicon glyphicon-thumbs-up" aria-hidden="true"></span>
						</div>
					</div>     

					<div class="col-md-4  project-column">
						<div class="col-md-12  project-card">
							<img src="../img/u22.png" class="idea-img-preview">

							<h3>Lorem ipsum dolor a sit amet, constectetur a dipiscing elit.</h3>
						   
						   <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et vi.</p>
							<span class="glyphicon glyphicon-thumbs-up" aria-hidden="true"></span>
						</div>
					</div>   
		
					<div class="col-md-4 project-column">
						<div class="col-md-12  project-card">
							<a href="idea-detail.php    ">
								<img src="../img/u22.png" class="idea-img-preview">

								<h3>Lorem ipsum dolor a sit amet, constectetur a dipiscing elit.</h3>
							</a>
						   
						   <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et vi.</p>

						   <span class="glyphicon glyphicon-thumbs-up" aria-hidden="true"></span>
						</div>
					</div>    

					<div class="col-md-4 project-column">
						<div class="col-md-12  project-card">
							<a href="idea-detail.php">
								<img src="../img/u22.png" class="idea-img-preview">

								<h3>Lorem ipsum dolor a sit amet, constectetur a dipiscing elit.</h3>
							</a>
						   
						   <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et vi.</p>

						   <span class="glyphicon glyphicon-thumbs-up" aria-hidden="true"></span>
						</div>
					</div>     
					<!--   / Copy Pasta --> 
				<!-- Projects holder end -->
				</div>

			<!-- ROW END -->
			</div>
			<!-- endrow projects -->
		
			<!-- Start row end page pagination -->
			<div class="row">
				<div class="col-md-12" id="overview-filters">

					<!-- pagination -->
					<div class="col-md-3 col-md-offset-5">
							<ul class="pagination" id="overview-pagination">
								<li class="disabled">
								  <span>
									<span aria-hidden="true">&laquo;</span>
								  </span>
								</li>
								<li class="active">
								  <span>1 <span class="sr-only">(current)</span></span>
								</li>
								<li>
									<span>2</span>
								</li>
								<li>
									<span>3</span>
								</li>
								<li>
									<span>4</span>
								</li>
								<li>
									<a href="#" aria-label="Next">
										<span aria-hidden="true">&raquo;</span>
									</a>
								</li>
							</ul>
					</div><!-- END PAGINATION -->
			<!-- End overview filters -->
			</div>
			<!-- end row end page pagination -->
			</div>

		<!-- Container end -->
		</div>	   
	<!-- main end -->
	</div>
	

	  <?php include('html-includes/footer.html'); ?>
	</body>
</hmtl>