<!DOCTYPE html>
<html>
    <head>
        <title>Idea detail | Epic Collaboration Zone</title>
       
        <link rel="shortcut icon" type="image/png" href="../img/favicon.png"/>    
        
        <!-- user stylesheets -->
        <link href='https://fonts.googleapis.com/css?family=Ubuntu:400,700italic,500italic,400italic,300italic,700,500,300' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" type="text/css" href="../css/custom.css">
        
        <!-- Bower stylesheets -->
        <link href="../bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" type="text/css">

        <!-- Bower scripts -->
        <script src="../bower_components/jquery/dist/jquery.min.js"></script>
        <script src="../bower_components/bootstrap/dist/js/bootstrap.js"></script>

    </head>
<body>
    <?php include('html-includes/navigation-nomenu.html'); ?>

    <div class="main">
        <div class="container">
            <!--   startrow details   -->
            <div class="row">   
                <!-- Project Details -->
                <div class="col-md-8">
                    <div class="col-md-12 project-details">
                        <h2>Project title</h2>
                        <div class="row">
                            <div class="col-sm-4">
                                <img src="../img/u22.png">
                            </div>
                            <div class="col-sm-8"><h4>Mission statement</h4>
                            <p>Lorem ipsum dolor a sit amet, constectetur a dipiscing elit.</p>
                            <h4>Description</h4>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et vi. Phasellus dignissim eu libero in gravida. Pellentesque ac molestie nibh, quis auctor dui. Donec convallis leo ac dui tincidunt, non lacinia lectus maximus.</p>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et vi. Phasellus dignissim eu libero in gravida. Pellentesque ac molestie nibh, quis auctor dui. Donec convallis leo ac dui tincidunt, non lacinia lectus maximus.</p>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- End Project Details -->
                
                <!-- Project owner details -->
                <div class="col-md-4">
                    <div class="col-md-12 project-owner">
                        <h2>Project owner</h2>
                        <div class="row owner">
                            <div class="col-xs-3">
                                <img src="../img/user-placeholder.png">
                            </div>
                            <div class="col-xs-9">
                                John Doe<br>
                                <a href="#">See more ideas</a>
                            </div>
                        </div>
                            
                        <p>Posted: February 1, 2016<br>Status: Submitted</p>
                        <p><a href="#"><span class="glyphicon glyphicon-envelope"></span>Share this idea</a></p>
                        <button onclick="#" class="button-sq-md"><span class="glyphicon glyphicon-star"></span>I'm interested</button>
                    </div>             
                </div>
                <!-- End Project owner details -->
            </div>
        </div>
    </div>    
 <?php include('html-includes/footer.html'); ?>
</body>
</hmtl>