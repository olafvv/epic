<!DOCTYPE html>
<html>
    <head>
        <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
        <link rel="stylesheet" type="text/css" href="css/custom.css">
        <link href='https://fonts.googleapis.com/css?family=Ubuntu:400,700italic,500italic,400italic,300italic,700,500,300' rel='stylesheet' type='text/css'>
        <link rel="shortcut icon" type="image/png" href="img/favicon.png"/>

        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <script type="text/javascript" src="./js/script.js"></script>
        <script type="text/javascript" src="./js/navigation.js"></script>
        <title>Epic Collaboration Zone</title>
    </head>
    
    <body>
    <div class="container">
        
        <div class="main" style="background-image:url(img/login-bg.png); background-attachment:scroll;"
            <!-- NAVIGATION -->
            <nav class="nav navbar navbar-fixed-top">
                <a href="index.php"><img class="logo left" src="img/logo.png"></a>
            </nav>
            


            <div class="welcome right">
                <h2>Welcome to Epic</h2>
                <h4>where ideas become reality</h4>
                <div class="col-lg-5 login">
                    <form id="login" class="myForm" action="" method="POST">
                        <input type="email" name="email" placeholder="Email" class="form-group text"><br>
                        <input type="password" name="password" placeholder="Password" class="form-group text"><br>
                        <a href="pages/password-recovery.php">Forgot your password?</a><br>
                        <input type="submit" name="submit" value="Login" class="form-group register-button submit">
                    </form>
                </div>
                <div class="col-lg-1"></div>
                <div class="col-lg-5 register">
                    <h4>Not yet an account?<br>Sign up and share your idea!</h4>
                    <a href="pages/register.php" class="register-button">Create your account</a>
                </div>
            </div>
            
        </div>
        
        <div class="footer">
            <div class="col-sm-4">
                <ul class="nav">
                    <li><a href="">About Collaboration Zone</a></li>
                    <li><a href="">Contact information</a></li>
                    <li><a href="">Stuff people expect in the footer</a></li>
                </ul>
            </div>

            <div class="col-sm-4">
            </div>

            <div class="col-sm-4">
                <img src="img/footer-logo.png" class="footer-img">
            </div>
        </div>
        
    </div>    
    </body>
</html>