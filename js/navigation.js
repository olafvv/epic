($(document).ready(function(){

	//Add listener for the navigation toggle button.
	// If the navigation is expanded add class expanded,
	// else removeCLass
	$('.navbar-toggle').click(function(e){
		if($('.navbar-collapse').hasClass('expanded') == true){
			$('.navbar-collapse').removeClass('expanded');
		}else{
			$('.navbar-collapse').addClass('expanded');
		}
	});

	//loop through all elements in the navbar and remove the element
	$(".navbar-fixed-top li" ).each(function(index, element){
		$(element).removeClass();
	});

	//based on the location, get the id of the element and add active class
	var location = window.location.href;
	var page 	 = location.substring(location.lastIndexOf('/')+1, location.lastIndexOf('.'));
	var nav_elem = "#nav-"+page;
	// console.log(nav_elem);

	$(nav_elem).addClass('active');
}));