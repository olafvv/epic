($(document).ready(function(){
	 
	//Signup form
	$("#register").submit(function(e){
		console.log(e);

		e.preventDefault();
		$(".form_error").remove();
		
		var name = $("input[name='name']").val();
		var email = $("input[name='email']").val();
		var password = $("input[name='password']").val();
		var password_confirm = $("input[name='password-confirm']").val();
		var role = $("input[name='role']").val();
		var terms = $("input[name='terms']").val();
		
		if(name == "" || name.length < 3 || name.length > 100){
			$("input[name='name']").after("<p class='form_error'>Please fill in a correct name</p>");
			return false;
		}
		if(email == "" || email.length < 3 || email.length > 100){
			$("input[name='email']").after("<p class='form_error'>Please fill in a correct email address</p>");
			return false;
		}
		if(!validateEmail(email)){
			$("input[name='email']").after("<p class='form_error'>Please enter a valid email address</p>");
			return false;
		}
		
		if(password == "" || password.length < 6 || name.length > 100){
			$("input[name='password']").after("<p class='form_error'>Please fill in a correct password, at least 6 characters</p>");
			return false;
		}
		if(password != password_confirm){
			$("input[name='password-confirm']").after("<p class='form_error'>Passwords don't match</p>");
			return false;
		}
	});
	
	function validateEmail(email) {
		var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
		return re.test(email);
	};

	
}));